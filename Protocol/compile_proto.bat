SET CS=../"Client"
SET CPP=../"Server"

protoc protocol.proto --cpp_out=%CPP% --csharp_out=%CS%
if errorlevel 1 (
   pause
)
