﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.GameWindow_Pages
{
    /// <summary>
    /// Interaction logic for RoomCreator.xaml
    /// </summary>
    public partial class RoomCreator : Page
    {
        public RoomCreator()
        {
            InitializeComponent();
            InitializePacksDataGrid();
        }

        private class PackRow
        {
            public bool IsSelected { get; set; }
            public Protocol.PackInfo PackInfo { get; set; }
        }

        private async Task InitializePacksDataGrid()
        {
            try
            {
                // Get packs from user
                var reply = await TriviaClient.SendMessage<Protocol.PacksList>(Protocol.ClientMessageCode.Types.Code.RequestPacks);
                if (await reply.Item1 == Protocol.ServerReplyCode.Types.Code.Ok)   // If OK
                {
                    Protocol.PacksList packsList = await reply.Item2;

                    PacksDataGrid.ItemsSource =
                        (from pack in packsList.Packs
                         select new PackRow() { PackInfo = pack }).ToList();
                }
                else    // If Error
                    throw new Exception("Error loading packs.");
            }
            catch (Exception exc)
            {
                ErrorLabel.Content = exc.Message;
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private async void CreateRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomButton.IsEnabled = false;
            try
            {
                // Clear error label
                ErrorLabel.Content = "";

                // Check if question count is ok
                int maxQuestions =
                    (from packRow in PacksDataGrid.Items.Cast<PackRow>()
                     where packRow.IsSelected
                     select packRow.PackInfo.QuestionCount).Sum();
                if (int.Parse(QuestionCountTextBox.Text) > maxQuestions)
                    throw new Exception("You can't have more questions than available.");

                // Check for text in the room's name
                if (RoomNameTextBox.Text.Length == 0)
                    throw new Exception("You have to specify the room's name.");

                // Make the RoomInfo
                Protocol.RoomInfo roomInfo = new Protocol.RoomInfo()
                {
                    Name = RoomNameTextBox.Text,
                    MaxUsers = int.Parse(MaxUsersTextBox.Text),
                    QuestionCount = int.Parse(QuestionCountTextBox.Text)
                };

                // Get selected packs
                foreach (var packRow in PacksDataGrid.ItemsSource.Cast<PackRow>())
                {
                    if (packRow.IsSelected)
                        roomInfo.Packs.Add(new Protocol.PackInfo() { Id = packRow.PackInfo.Id });
                }
                if (roomInfo.Packs.Count == 0)
                    throw new Exception("You have to select at least one pack.");

                // Send to server
                var reply = await TriviaClient.SendMessage(Protocol.ClientMessageCode.Types.Code.CreateRoom, roomInfo);

                if (reply == Protocol.ServerReplyCode.Types.Code.Ok)
                    NavigationService.Navigate(new RoomLobby(true, roomInfo, new Protocol.UsersList()));
                else
                    throw new Exception("An error has occured.");
            }
            catch (Exception exc)
            {
                ErrorLabel.Content = exc.Message;
            }
            finally
            {
                CreateRoomButton.IsEnabled = true;
            }
        }
    }
}
