﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.GameWindow_Pages
{
    /// <summary>
    /// Interaction logic for RoomBrowser.xaml
    /// </summary>
    public partial class RoomBrowser : Page
    {
        public RoomBrowser()
        {
            InitializeComponent();

            InitializeRoomsDataGrid();
        }

        private class RoomInfoRow
        {
            public Protocol.RoomInfo RoomInfo;
            public string PacksString;
        }

        private async Task InitializeRoomsDataGrid()
        {
            RoomsDataGrid.IsEnabled = false;
            try
            {
                var reply = await TriviaClient.SendMessage<Protocol.RoomsList>(Protocol.ClientMessageCode.Types.Code.RequestRooms);
                if (await reply.Item1 == Protocol.ServerReplyCode.Types.Code.Ok)
                {
                    Protocol.RoomsList list = await reply.Item2;
                    RoomsDataGrid.ItemsSource =
                        from roomInfo in list.Rooms
                        select new RoomInfoRow()
                        {
                            RoomInfo = roomInfo,
                            PacksString = String.Join(",", from packInfo in roomInfo.Packs
                                                         select packInfo.Name)
                        };
                }
                else
                    throw new Exception("An error has occured.");
            }
            catch (Exception exc)
            {
                ErrorLabel.Content = exc.Message;
            }
            finally
            {
                RoomsDataGrid.IsEnabled = true;
            }
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshButton.IsEnabled = false;
            try
            {
                ErrorLabel.Content = "";
                await InitializeRoomsDataGrid();
            }
            finally
            {
                RefreshButton.IsEnabled = true;
            }
        }

        private async void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            RoomsDataGrid.IsEnabled = false;
            try
            {
                Protocol.RoomInfo roomInfo = ((sender as DataGridRow).Item as RoomInfoRow).RoomInfo;
                // Send EnterRoom
                var reply = await TriviaClient.SendMessage<Protocol.UsersList>(Protocol.ClientMessageCode.Types.Code.EnterRoom, new Protocol.RoomInfo() { Id = roomInfo.Id });
                if (await reply.Item1 == Protocol.ServerReplyCode.Types.Code.Ok)
                    NavigationService.Navigate(new RoomLobby(false, roomInfo, await reply.Item2));
                else
                    throw new Exception("An error has occured.");
            }
            catch (Exception exc)
            {
                ErrorLabel.Content = exc.Message;
            }
            finally
            {
                RoomsDataGrid.IsEnabled = true;
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
