﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.GameWindow_Pages
{
    /// <summary>
    /// Interaction logic for RoomLobby.xaml
    /// </summary>
    public partial class RoomLobby : Page
    {
        Protocol.RoomInfo RoomInfo;
        CancellationTokenSource cts = new CancellationTokenSource();

        public RoomLobby(bool isAdmin, Protocol.RoomInfo roomInfo, Protocol.UsersList usersList)
        {
            InitializeComponent();
            // Get the right view
            if (isAdmin)
                AdminGrid.Visibility = Visibility.Visible;
            else
                NonAdminGrid.Visibility = Visibility.Visible;

            UsersDataGrid.ItemsSource = usersList.Users;
            RoomInfo = roomInfo;    // TODO - more room info on screen
            RecvUpdatesFromServer(cts.Token);
        }

        private async void RecvUpdatesFromServer(CancellationToken ct)
        {
            try
            {
                while (true)
                {
                    var reply = await TriviaClient.RecvMessageCode(ct);
                    switch (reply)
                    {
                        case Protocol.ServerMessageCode.Types.Code.PlayerJoined:
                            UsersDataGrid.Items.Add(await TriviaClient.RecvMessageCodeParams<Protocol.UserInfo>());
                            break;

                        case Protocol.ServerMessageCode.Types.Code.PlayerLeft:
                            RemovePlayerByUsername((await TriviaClient.RecvMessageCodeParams<Protocol.UserInfo>()).Username);
                            break;

                        case Protocol.ServerMessageCode.Types.Code.StartGame:
                            NavigationService.Navigate(new TriviaGame(RoomInfo));
                            return;
                    }
                }
            }
            catch { }
        }

        private void RemovePlayerByUsername(string username)
        {
            UsersDataGrid.Items.Remove(
                from item in UsersDataGrid.Items.Cast<Protocol.UserInfo>()
                where item.Username == username
                select item);
        }

        private async void BackButton_Click(object sender, RoutedEventArgs e)
        {
            await TriviaClient.SendMessage(Protocol.ClientMessageCode.Types.Code.LeaveRoom);
            NavigationService.GoBack();
        }

        private async void StartGameButton_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
            await TriviaClient.SendMessage(Protocol.ClientMessageCode.Types.Code.ReadyToStartGame);
            NavigationService.Navigate(new TriviaGame(RoomInfo));
        }
    }
}
