﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.GameWindow_Pages
{
    /// <summary>
    /// Interaction logic for TriviaGame.xaml
    /// </summary>
    public partial class TriviaGame : Page
    {
        Protocol.RoomInfo RoomInfo;

        public TriviaGame(Protocol.RoomInfo roomInfo)
        {
            InitializeComponent();
            RoomInfo = roomInfo;

            GameStartCountdownLabel.CountSeconds(10, 0);
            WaitForStartGame();
        }

        async Task WaitForStartGame()
        {
            for (int rounds = 0; rounds < RoomInfo.QuestionCount; rounds++)
            {
                // Wait to get StartRound
                var reply = await TriviaClient.RecvMessageCode();
                BeforeStartGameGrid.Visibility = Visibility.Hidden;
                GameGrid.Visibility = Visibility.Visible;
                if (reply == Protocol.ServerMessageCode.Types.Code.StartRound)
                {
                    var questionInfo = await TriviaClient.RecvMessageCodeParams<Protocol.QuestionInfo>();
                    QuestionTextBox.Text = questionInfo.Question;
                    ((Answer1Button.Template.LoadContent() as Border).Child as TextBox).Text = questionInfo.Answer1;
                    ((Answer2Button.Template.LoadContent() as Border).Child as TextBox).Text = questionInfo.Answer2;
                    ((Answer3Button.Template.LoadContent() as Border).Child as TextBox).Text = questionInfo.Answer3;
                    ((Answer4Button.Template.LoadContent() as Border).Child as TextBox).Text = questionInfo.Answer4;
                }
            }
        }
    }
}
