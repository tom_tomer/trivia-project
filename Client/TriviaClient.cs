﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Configuration;
using Google.Protobuf;
using ClientMessageCodeEnum = Protocol.ClientMessageCode.Types.Code;
using ServerReplyCodeEnum = Protocol.ServerReplyCode.Types.Code;
using ServerMessageCodeEnum = Protocol.ServerMessageCode.Types.Code;

namespace Client
{
    static class TriviaClient
    {
        private static string ip = ConfigurationManager.AppSettings.Get("server-ip");
        private static int port = int.Parse(ConfigurationManager.AppSettings.Get("port"));

        private static TcpClient TcpClient;

        // Sends a byte array with its length
        private static async Task SendWithSize(byte[] bytes)
        {
            if (TcpClient == null)
                throw new Exception("'TriviaClient.Connect()' was not called.");

            byte[] length = BitConverter.GetBytes(bytes.Length);
            byte[] message = Utils.Combine(length, bytes);
            await TcpClient.GetStream().WriteAsync(message, 0, message.Length);
        }

        // Recieves a string with its length
        private static async Task<byte[]> RecvWithSize(CancellationToken ct = default(CancellationToken))
        {
            if (TcpClient == null)
                throw new Exception("'TriviaClient.Connect()' was not called.");

            byte[] length = new byte[sizeof(int)];
            await TcpClient.GetStream().ReadAsync(length, 0, length.Length, ct);
            byte[] bytes = new byte[BitConverter.ToInt32(length, 0)];
            if (bytes.Length != 0)
                await TcpClient.GetStream().ReadAsync(bytes, 0, bytes.Length, ct);
            return bytes;
        }

        // Sends client message code to server
        private static async Task SendMessageCode(ClientMessageCodeEnum code)
        {
            Protocol.ClientMessageCode message = new Protocol.ClientMessageCode() { Code = code };
            await SendWithSize(message.ToByteArray());
        }

        // Gets server message code from server
        private static async Task<ServerReplyCodeEnum> RecvReplyCode()
        {
            return Protocol.ServerReplyCode.Parser.ParseFrom(await RecvWithSize()).Code;
        }

        private static async Task SendParams(IMessage message)
        {
            await SendWithSize(message.ToByteArray());
        }

        private static async Task<T> RecvReturnMessage<T>() where T : IMessage<T>, new()
        {
            MessageParser<T> parser = new MessageParser<T>(() => { return new T(); });
            return parser.ParseFrom(await RecvWithSize());
        }

        // ---------------------------------------------------------------------------------------------------------- //

        // Connect to the server
        public static async Task Connect()
        {
            TcpClient = new TcpClient();
            await TcpClient.ConnectAsync(ip, port);
        }

        // Send message and don't get return message
        public static async Task<ServerReplyCodeEnum> SendMessage(ClientMessageCodeEnum code, IMessage @params = null)
        {
            await SendMessageCode(code);
            if (@params != null)
                await SendParams(@params);
            return await RecvReplyCode();
        }
        
        // Send message and get return message
        public static async Task<Tuple<Task<ServerReplyCodeEnum>, Task<TReturn>>> SendMessage<TReturn>(ClientMessageCodeEnum code, IMessage @params = null) where TReturn : IMessage<TReturn>, new()
        {
            await SendMessageCode(code);
            if (@params != null)
                await SendParams(@params);

            var task1 = RecvReplyCode();
            Func<Task<TReturn>> temp = async () => { await task1; return await RecvReturnMessage<TReturn>(); };
            var task2 = temp();
            return new Tuple<Task<ServerReplyCodeEnum>, Task<TReturn>>(task1, task2);
        }

        // Gets server reply code from server
        public static async Task<ServerMessageCodeEnum> RecvMessageCode(CancellationToken ct = default(CancellationToken))
        {
            return Protocol.ServerMessageCode.Parser.ParseFrom(await RecvWithSize(ct)).Code;
        }

        // Get params of server message code
        public static async Task<TReturn> RecvMessageCodeParams<TReturn>() where TReturn : IMessage<TReturn>, new()
        {
            return await RecvReturnMessage<TReturn>();
        }

        // Send Disconnect message code
        public static async Task Disconnect()
        {
            await SendMessageCode(ClientMessageCodeEnum.Disconnect);
            TcpClient.Close();
        }
    }
}
