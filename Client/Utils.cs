﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Client
{
    static class Utils
    {
        public static byte[] Combine(params byte[][] arrays)
        {
            byte[] rv = new byte[arrays.Sum(a => a.Length)];
            int offset = 0;
            foreach (byte[] array in arrays)
            {
                System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            return rv;
        }

        public static async Task CountSeconds(this Label label, int from, int to)
        {
            int add = (to - from) > 0 ? 1 : -1;
            for (int i = from; i != to; i += add)
            {
                label.Content = i.ToString();
                await Task.Delay(1000);
            }
            label.Content = to.ToString();
            await Task.Delay(1000);
        }
    }
}
