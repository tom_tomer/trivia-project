﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Net.Sockets;
using ClientMessageCodeEnum = Protocol.ClientMessageCode.Types.Code;
using ServerReplyCodeEnum = Protocol.ServerReplyCode.Types.Code;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginRegisterWindow : Window
    {
        private Grid CurrGrid;

        public LoginRegisterWindow()
        {
            InitializeComponent();
            CurrGrid = MainGrid;
        }

        private async void LoginSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            LoginErrorLabel.Content = "";
            LoginBackButton.IsEnabled = false;
            LoginSubmitButton.IsEnabled = false;
            LoginUsername.IsEnabled = false;
            LoginPassword.IsEnabled = false;
            // Show loading icon
            (LoginSubmitButton.Content as Grid).Children[0].Visibility = Visibility.Hidden;
            (LoginSubmitButton.Content as Grid).Children[1].Visibility = Visibility.Visible;
            try
            {
                // Set up UserInfo message
                Protocol.UserInfo userInfo = new Protocol.UserInfo()
                {
                    Username = LoginUsername.Text,
                    Password = LoginPassword.Password
                };

                // Connect to server and send UserInfo
                await TriviaClient.Connect();
                var reply = await TriviaClient.SendMessage(ClientMessageCodeEnum.SignIn, userInfo);
                // Get response from server
                ServerReplyCodeEnum response = reply;
                switch (response)
                {
                    case ServerReplyCodeEnum.Ok:  // Logged in
                        DialogResult = true;
                        Close();
                        break;

                    case ServerReplyCodeEnum.Failed:  // User info incorrect
                        LoginErrorLabel.Content = "Username or password is incorrect. Please try again.";
                        break;

                    case ServerReplyCodeEnum.Error:   // Error
                        LoginErrorLabel.Content = "An error has occured. Please try again.";
                        break;
                }
            }
            catch   // If can't connect to server
            {
                LoginErrorLabel.Content = "Can't connect to the server. Please check your internet connection.";
            }
            finally
            {
                LoginBackButton.IsEnabled = true;
                LoginSubmitButton.IsEnabled = true;
                LoginUsername.IsEnabled = true;
                LoginPassword.IsEnabled = true;
                // Turn off loading icon
                (LoginSubmitButton.Content as Grid).Children[0].Visibility = Visibility.Visible;
                (LoginSubmitButton.Content as Grid).Children[1].Visibility = Visibility.Hidden;
            }
        }

        private async void RegisterSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            // Check for password match
            if (!RegisterPassword.Password.Equals(RegisterReenterPassword.Password))
            {
                RegisterErrorLabel.Content = "Both passwords do not match.";
                return;
            }

            RegisterErrorLabel.Content = "";
            RegisterBackButton.IsEnabled = false;
            RegisterSubmitButton.IsEnabled = false;
            RegisterUsername.IsEnabled = false;
            RegisterPassword.IsEnabled = false;
            RegisterReenterPassword.IsEnabled = false;
            // Show loading icon
            (RegisterSubmitButton.Content as Grid).Children[0].Visibility = Visibility.Hidden;
            (RegisterSubmitButton.Content as Grid).Children[1].Visibility = Visibility.Visible;
            try
            {
                // Set up UserInfo message
                Protocol.UserInfo userInfo = new Protocol.UserInfo()
                {
                    Username = RegisterUsername.Text,
                    Password = RegisterPassword.Password
                };

                // Connect to server and send UserInfo
                await TriviaClient.Connect();
                var reply = await TriviaClient.SendMessage(ClientMessageCodeEnum.Register, userInfo);
                // Get response from server
                switch (reply)
                {
                    case ServerReplyCodeEnum.Ok:  // Registered
                        DialogResult = true;
                        Close();
                        break;

                    case ServerReplyCodeEnum.Failed:  // User info incorrect
                        RegisterErrorLabel.Content = "User already exists. Try a different username.";
                        break;

                    case ServerReplyCodeEnum.Error:   // Error
                        RegisterErrorLabel.Content = "An error has occured. Please try again.";
                        break;
                }
            }
            catch   // If can't connect to server
            {
                RegisterErrorLabel.Content = "Can't connect to the server. Please check your internet connection.";
            }
            finally
            {
                RegisterBackButton.IsEnabled = true;
                RegisterSubmitButton.IsEnabled = true;
                RegisterUsername.IsEnabled = true;
                RegisterPassword.IsEnabled = true;
                RegisterReenterPassword.IsEnabled = true;
                // Turn off loading icon
                (RegisterSubmitButton.Content as Grid).Children[0].Visibility = Visibility.Visible;
                (RegisterSubmitButton.Content as Grid).Children[1].Visibility = Visibility.Hidden;
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.Visibility = Visibility.Hidden;
            LoginGrid.Visibility = Visibility.Visible;
            CurrGrid = LoginGrid;
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.Visibility = Visibility.Hidden;
            RegisterGrid.Visibility = Visibility.Visible;
            CurrGrid = RegisterGrid;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            CurrGrid.Visibility = Visibility.Hidden;
            MainGrid.Visibility = Visibility.Visible;
            CurrGrid = MainGrid;

            LoginErrorLabel.Content = "";
            RegisterErrorLabel.Content = "";
        }
    }
}
