﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                // Open login/register window
                Window loginWindow = new LoginRegisterWindow();

                if (loginWindow.ShowDialog() == false)   // If closed the window
                {
                    Shutdown();
                }
                
                // Open actual game
                Window gameWindow = new GameWindow();
                gameWindow.ShowDialog();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            Shutdown();
        }

        private async void Application_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                await TriviaClient.Disconnect();
            }
            catch { }
        }
    }
}
