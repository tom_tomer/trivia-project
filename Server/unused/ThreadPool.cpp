#include "ThreadPool.h"
#include <iostream>

std::unique_ptr<std::thread> ThreadPool::getNewThread()
{
	std::cout << "1\n";
	std::unique_ptr<std::thread> t(new std::thread([&] 
	{
		std::cout << "Thread created.\n";
		while (true)
		{
			// wait for wake up
			std::unique_lock<std::mutex> lock(mutex);
			while (tasks.empty())
				cv.wait(lock);

			if (!tasks.empty())	// if woken up for a task
			{
				auto task = tasks.front();
				tasks.pop();
				lock.unlock();
				task();
			}
		}
	}));

	return t;
}

ThreadPool::ThreadPool(int capacity) : threads(capacity)
{
	// fill the vector with sleeping threads
	for (int i = 0; i < capacity; i++)
	{
		threads[i] = getNewThread();
	}
}

void ThreadPool::push(std::function<void()> task)
{
	tasks.push(task);
	cv.notify_one();
}
