#pragma once
#include <thread>
#include <mutex>
#include <vector>
#include <queue>
#include <memory>

class ThreadPool
{
private:
	std::vector<std::unique_ptr<std::thread>> threads;
	std::queue<std::function<void()>> tasks;
	std::mutex mutex;
	std::condition_variable cv;

	// Creates a sleeping thread and returns it.
	std::unique_ptr<std::thread> getNewThread();

public:
	ThreadPool(int capacity);

	// Pushes a function to the queue and wakes up a thread to do it.
	void push(std::function<void()> task);
};