#include "Config.h"
#include "Utils.h"
#include <fstream>

// Constants
const std::string Config::path = "config.txt";

Config::Config()
{
	std::ifstream file(path);
	if (!file.is_open())
		throw std::exception("No config file found.");

	std::string line;
	while (std::getline(file, line))
	{
		auto pieces = utils::split(line, '=');
		values[pieces[0]] = pieces[1];
	}
}

Config &Config::get()
{
	static Config instance;

	return instance;
}

std::string Config::get(const std::string &key)
{
	auto it = values.find(key);
	if (it == values.end())
		throw std::exception("Config: key not found.");

	return it->second;
}
