#pragma once
#define _WINSOCKAPI_
#include <exception>
#include <string>
#include <WinSock2.h>
#include <Windows.h>

class SocketException : public std::exception
{
private:
	std::string getWSAError()
	{
		wchar_t *s = NULL;
		FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, WSAGetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPWSTR)&s, 0, NULL);
		std::wstring wstr(s);
		std::string str(wstr.begin(), wstr.end());
		LocalFree(s);
		return str;
	}

public:
	SocketException(const std::string &message) : std::exception((message + '\n' + getWSAError()).c_str()) { }
};