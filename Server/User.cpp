#include "User.h"

User::User(SOCKET socket) : socket_(socket) { }

std::string User::getUsername() const
{
	return username;
}

void User::setUsername(const std::string &username)
{
	this->username = username;
}

Room& User::getRoom()
{
	return *room;
}

bool User::isRoomOwner()
{
	return getRoom().getOwner() == socket_;
}
