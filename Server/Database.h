#pragma once
#include "protocol.pb.h"
#include <SQLiteCpp/SQLiteCpp.h>
#include <string>

class Database
{
private:
	// Constants
	static const std::string path;

	// Singleton
	Database();

	SQLite::Database db;
	
	void createTableUsers();
	void createTablePacks();
	void createTableQuestion();

	void populatePacksAndQuestions();

public:
	static Database& get();

	bool userExists(const std::string& username);
	bool checkCredentials(const std::string& username, const std::string& password);
	bool addClient(const std::string& username, const std::string& password);

	std::string getPackNameById(int packId);
	void getPacksInfo(protocol::PacksList &list);
	int getPackQuestionCount(int packId);
	void getRandomQuestions(std::vector<std::pair<protocol::QuestionInfo, int>>& questions, const std::vector<int>& packIds);
};