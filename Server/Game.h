#pragma once
#include "protocol.pb.h"
#include "User.h"
#include "Room.h"
#include <WinSock2.h>
#include <string>
#include <map>
#include <list>
#include <mutex>
#include <memory>

class Room;
class User;

class Game
{
private:
	Game();

public:
	static Game& get();

	std::map<SOCKET, User> clients;
	std::mutex clients_mutex;
	std::list<Room> rooms;
	std::mutex rooms_mutex;

	void addClient(SOCKET clientSocket);

	void handleSignIn(SOCKET clientSocket, std::shared_ptr<protocol::UserInfo> userInfo);
	void handleRegister(SOCKET clientSocket, std::shared_ptr<protocol::UserInfo> userInfo);
	void handleCreateRoom(SOCKET clientSocket, std::shared_ptr<protocol::RoomInfo> roomInfo);
	void handleRequestRooms(SOCKET clientSocket);
	void handleRequestPacks(SOCKET clientSocket);
	void handleReadyToStartGame(SOCKET clientSocket);
	void handleEnterRoom(SOCKET clientSocket, std::shared_ptr<protocol::RoomInfo> roomInfo);
	void handleLeaveRoom(SOCKET clientSocket);
	void handleSendAnswer(SOCKET clientSocket, std::shared_ptr<protocol::QuestionInfo> questionInfo);
};