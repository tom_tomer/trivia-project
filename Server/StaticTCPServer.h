#pragma once
#include "TCPServer.h"
#include "protocol.pb.h"
#include <string>

class StaticTCPServer
{
private:
	static TCPServer serverSocket;

	static void sendWithSize(SOCKET clientSocket, std::string data);
	static std::string recvWithSize(SOCKET clientSocket);

public:
	static void bind(const std::string &ip, short port);
	static void listen(int backlog = SOMAXCONN);
	static SOCKET accept();
	static bool isThereData(SOCKET clientSocket);

	static void sendMessageCode(SOCKET clientSocket, protocol::ServerMessageCode::Code code);
	static void sendReplyCode(SOCKET clientSocket, protocol::ServerReplyCode::Code code);
	static protocol::ClientMessageCode::Code recvMessageCode(SOCKET clientSocket);

	static void sendMessage(SOCKET clientSocket, const google::protobuf::Message &message);
	template <typename T>
	static std::shared_ptr<T> recvMessage(SOCKET clientSocket)
	{
		static_assert(std::is_base_of<google::protobuf::Message, T>::value);
		std::shared_ptr<T> message(new T);
		message.get()->ParseFromString(recvWithSize(clientSocket));
		std::cout << "Recieved message " << typeid(*(message.get())).name() << ".\n";
		return message;
	}
};
