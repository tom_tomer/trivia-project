#pragma once
#include "protocol.pb.h"
#include "Room.h"
#include <WinSock2.h>
#include <memory>
#include <list>

class Room;
class Game;

class User
{
	friend Room;
private:
	std::string username;
	Room* room;

public:
	SOCKET socket_;

	User(SOCKET socket);

	std::string getUsername() const;
	void setUsername(const std::string &username);
	Room& getRoom();
	bool isRoomOwner();
};