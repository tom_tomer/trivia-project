#include "TriviaServer.h"
#include "Config.h"
#include "Utils.h"
#include <chrono>
#include <iostream>

using Server = StaticTCPServer;

TriviaServer &TriviaServer::get()
{
	static TriviaServer instance;
	return instance;
}

void TriviaServer::handleClientMessageCode(SOCKET clientSocket, protocol::ClientMessageCode::Code messageCode)
{
	// get the actual message based on message code
	switch (messageCode)
	{
		// SIGN_IN
		case protocol::ClientMessageCode::SIGN_IN:
			pool.enqueue(&Game::handleSignIn, std::ref(Game::get()), clientSocket, Server::recvMessage<protocol::UserInfo>(clientSocket));
			break;

		// REGISTER
		case protocol::ClientMessageCode::REGISTER:
			pool.enqueue(&Game::handleRegister, std::ref(Game::get()), clientSocket, Server::recvMessage<protocol::UserInfo>(clientSocket));
			break;

		// CREATE_ROOM
		case protocol::ClientMessageCode::CREATE_ROOM:
			pool.enqueue(&Game::handleCreateRoom, std::ref(Game::get()), clientSocket, Server::recvMessage<protocol::RoomInfo>(clientSocket));
			break;

		// REQUEST_ROOMS
		case protocol::ClientMessageCode::REQUEST_ROOMS:
			pool.enqueue(&Game::handleRequestRooms, std::ref(Game::get()), clientSocket);
			break;

		// REQUEST_PACKS
		case protocol::ClientMessageCode::REQUEST_PACKS:
			pool.enqueue(&Game::handleRequestPacks, std::ref(Game::get()), clientSocket);
			break;

		// READY_TO_START_GAME	
		case protocol::ClientMessageCode::READY_TO_START_GAME:
			pool.enqueue(&Game::handleReadyToStartGame, std::ref(Game::get()), clientSocket);
			break;

		// ENTER_ROOM
		case protocol::ClientMessageCode::ENTER_ROOM:
			pool.enqueue(&Game::handleEnterRoom, std::ref(Game::get()), clientSocket, Server::recvMessage<protocol::RoomInfo>(clientSocket));
			break;

		// LEAVE_ROOM
		case protocol::ClientMessageCode::LEAVE_ROOM:
			pool.enqueue(&Game::handleLeaveRoom, std::ref(Game::get()), clientSocket);
			break;

		// SEND_ANSWER
		case protocol::ClientMessageCode::SEND_ANSWER:
			pool.enqueue(&Game::handleSendAnswer, std::ref(Game::get()), clientSocket, Server::recvMessage<protocol::QuestionInfo>(clientSocket));
			break;
	}
}

void TriviaServer::handleClients()
{
	while (true)
	{
		std::unique_lock<std::mutex> lock(Game::get().clients_mutex);
		// get messages from all clients
		for (auto &client : Game::get().clients)
		{
			SOCKET clientSocket = client.first;
			lock.unlock();

			if (Server::isThereData(clientSocket))	// if there is data
				handleClientMessageCode(clientSocket, Server::recvMessageCode(clientSocket));

			lock.lock();
		}

		// sleep a bit
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

TriviaServer::TriviaServer() : pool(20)
{
	Server::bind("0.0.0.0", stoi(Config::get().get("port")));

	// create a thread to handle all connected clients
	pool.enqueue(&TriviaServer::handleClients, this);
}

void TriviaServer::start()
{
	// handle new connections
	while (true)
	{
		// wait for client
		TRACELN("Waiting for a connection...");
		Server::listen(1);
		SOCKET clientSocket = Server::accept();
		TRACELN("New client connected.");
		Game::get().addClient(clientSocket);
	}
}