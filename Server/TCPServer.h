#pragma once
#include "exceptions/SocketException.h"
#include <WinSock2.h>
#include <string>

class TCPServer
{
private:
	SOCKET serverSocket;

public:
	TCPServer();
	~TCPServer();

	void bind(const std::string &ip, short port);
	void listen(int backlog = SOMAXCONN);
	SOCKET accept();
	// Send a message and return the number of bytes sent
	int send(SOCKET clientSocket, std::string message);	
	int sendSize(SOCKET clientSocket, uint32_t size);
	// Recieves a message with the specified amount of bytes
	std::string recv(SOCKET clientSocket, int bytes);
	uint32_t recvSize(SOCKET clientSocket);

	bool isThereData(SOCKET clientSocket);
};