#include "Utils.h"
#include <sstream>
#include <iterator>

namespace utils
{
	template<typename Out>
	void split(const std::string &s, char delim, Out result) 
	{
		std::stringstream ss;
		ss.str(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			*(result++) = item;
		}
	}

	std::vector<std::string> split(const std::string &s, char delim) 
	{
		std::vector<std::string> elems;
		split(s, delim, std::back_inserter(elems));
		return elems;
	}

	std::vector<int> getPacksId(const google::protobuf::RepeatedPtrField<protocol::PackInfo>& packs)
	{
		std::vector<int> ids;
		for (auto &pack : packs)
		{
			ids.push_back(pack.id());
		}
		return ids;
	}

	void trim(std::string &str)
	{
		rtrim(str);
		ltrim(str);
	}

	void rtrim(std::string &str)
	{
		size_t endpos = str.find_last_not_of(" \t");
		if (std::string::npos != endpos)
		{
			str = str.substr(0, endpos + 1);
		}
	}

	void ltrim(std::string &str)
	{
		size_t startpos = str.find_first_not_of(" \t");
		if (std::string::npos != startpos)
		{
			str = str.substr(startpos);
		}
	}
}