#include "Game.h"
#include "TriviaServer.h"
#include "StaticTCPServer.h"
#include "Database.h"
#include <iostream>
#include "Utils.h"

using Server = StaticTCPServer;

Game::Game() { }

Game &Game::get()
{
	static Game instance;
	return instance;
}

void Game::addClient(SOCKET clientSocket)
{
	std::unique_lock<std::mutex> lock(clients_mutex);
	clients.insert(std::make_pair(clientSocket, User(clientSocket)));
}

void Game::handleSignIn(SOCKET clientSocket, std::shared_ptr<protocol::UserInfo> userInfo)
{
	try
	{
		// Sign the client and send reply
		if (Database::get().checkCredentials(userInfo.get()->username(), userInfo.get()->password()))
		{
			std::unique_lock<std::mutex> lock(clients_mutex);
			clients.find(clientSocket)->second.setUsername(userInfo.get()->username());
			lock.unlock();
			Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
		}
		else
		{
			Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::FAILED);
		}
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleRegister(SOCKET clientSocket, std::shared_ptr<protocol::UserInfo> userInfo)
{
	try
	{
		// Add client and send reply
		if (Database::get().addClient(userInfo.get()->username(), userInfo.get()->password()))
		{
			std::unique_lock<std::mutex> lock(clients_mutex);
			clients.find(clientSocket)->second.setUsername(userInfo.get()->username());
			lock.unlock();
			Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
		}
		else
		{
			Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::FAILED);
		}
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleCreateRoom(SOCKET clientSocket, std::shared_ptr<protocol::RoomInfo> roomInfo)
{
	try
	{
		// Create room put it into list of rooms and send reply
		std::unique_lock<std::mutex> lock(rooms_mutex);
		rooms.emplace_back(clientSocket, *(roomInfo.get()));
		lock.unlock();
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleRequestRooms(SOCKET clientSocket)
{
	try
	{
		// Put all rooms into list
		protocol::RoomsList list;
		std::unique_lock<std::mutex> lock(rooms_mutex);
		for (auto &room : rooms)
			room.getRoomInfo(*list.add_rooms());
		lock.unlock();

		// Send reply
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
		Server::sendMessage(clientSocket, list);
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleRequestPacks(SOCKET clientSocket)
{
	try
	{
		// Get the PacksList
		protocol::PacksList list;
		Database::get().getPacksInfo(list);

		// Send reply
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
		Server::sendMessage(clientSocket, list);
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleReadyToStartGame(SOCKET clientSocket)
{
	try
	{
		std::unique_lock<std::mutex> lock(clients_mutex);
		auto &room = clients.find(clientSocket)->second.getRoom();
		lock.unlock();
		TriviaServer::get().pool.enqueue(&Room::startGame, std::ref(room));
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
	
}

void Game::handleEnterRoom(SOCKET clientSocket, std::shared_ptr<protocol::RoomInfo> roomInfo)
{
	try
	{ 
		// Go over the rooms until the needed one
		std::list<Room>::iterator room_it;
		std::unique_lock<std::mutex> lock(rooms_mutex);
		for (room_it = rooms.begin(); room_it != rooms.end(); room_it++)
		{
			if (room_it->getId() == roomInfo.get()->id())
				break;
		}
		lock.unlock();

		// Add to the needed room the client
		room_it->addUser(clientSocket);

		lock.lock();
		auto &usersInRoom = clients.find(clientSocket)->second.getRoom().users;
		lock.unlock();
		for (SOCKET socket : usersInRoom)
		{
			if (socket != clientSocket)
				Server::sendMessageCode(socket, protocol::ServerMessageCode::PLAYER_JOINED);
		}

		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleLeaveRoom(SOCKET clientSocket)
{
	try
	{
		std::unique_lock<std::mutex> lock(clients_mutex);
		auto &room = clients.find(clientSocket)->second.getRoom();
		lock.unlock();

		// Remove the client and send to all users in room message that he left
		room.removeUser(clientSocket);

		for (SOCKET socket : room.users)
		{
			Server::sendMessageCode(socket, protocol::ServerMessageCode::PLAYER_LEFT);
		}

		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::OK);
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}

void Game::handleSendAnswer(SOCKET clientSocket, std::shared_ptr<protocol::QuestionInfo> questionInfo)
{
	try
	{
		Server::sendMessageCode(clientSocket, protocol::ServerMessageCode::START_ROUND);
		Server::sendMessageCode(clientSocket, protocol::ServerMessageCode::END_ROUND);
		//Server::sendMessage(clientSocket, questionInfo.get()->correct_answer_num);
	}
	catch (std::exception& exc)
	{
		Server::sendReplyCode(clientSocket, protocol::ServerReplyCode::ERROR);
		std::cerr << "ERROR - " << exc.what();
	}
}