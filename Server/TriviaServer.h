#pragma once
#include "protocol.pb.h"
#include "StaticTCPServer.h"
#include "ThreadPool.h"
#include "Game.h"
#include <list>

class TriviaServer
{
private:
	TriviaServer();

	void handleClientMessageCode(SOCKET clientSocket, protocol::ClientMessageCode::Code messageCode);
	void handleClients();

public:
	static TriviaServer &get();

	ThreadPool pool;
	void start();
};