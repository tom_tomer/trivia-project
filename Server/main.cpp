#include "TriviaServer.h"
#include <iostream>

void main()
{
	try
	{
		TriviaServer::get().start();
	}
	catch (std::exception exc)
	{
		std::cerr << "ERROR - " << exc.what();
	}
	catch (...)
	{
		std::cerr << "ERROR";
	}

	getchar();
}