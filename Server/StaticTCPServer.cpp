#include "StaticTCPServer.h"
#include "Utils.h"
#include <iostream>

TCPServer StaticTCPServer::serverSocket;

void StaticTCPServer::sendWithSize(SOCKET clientSocket, std::string data)
{
	serverSocket.sendSize(clientSocket, data.size());
	serverSocket.send(clientSocket, data);
}

std::string StaticTCPServer::recvWithSize(SOCKET clientSocket)
{
	uint32_t length = serverSocket.recvSize(clientSocket);
	std::string message = serverSocket.recv(clientSocket, length);
	return message;
}

void StaticTCPServer::bind(const std::string &ip, short port)
{
	serverSocket.bind(ip, port);
}

void StaticTCPServer::listen(int backlog)
{
	serverSocket.listen(backlog);
}

SOCKET StaticTCPServer::accept()
{
	return serverSocket.accept();
}

bool StaticTCPServer::isThereData(SOCKET clientSocket)
{
	return serverSocket.isThereData(clientSocket);
}

void StaticTCPServer::sendMessageCode(SOCKET clientSocket, protocol::ServerMessageCode::Code code)
{
	protocol::ServerMessageCode message;
	message.set_code(code);
	sendWithSize(clientSocket, message.SerializeAsString());
	std::cout << "Sent message code " << code << ".\n";
}

void StaticTCPServer::sendReplyCode(SOCKET clientSocket, protocol::ServerReplyCode::Code code)
{
	protocol::ServerReplyCode message;
	message.set_code(code);
	sendWithSize(clientSocket, message.SerializeAsString());
	std::cout << "Sent message code " << code << ".\n";
}

protocol::ClientMessageCode::Code StaticTCPServer::recvMessageCode(SOCKET clientSocket)
{
	protocol::ClientMessageCode message;
	message.ParseFromString(recvWithSize(clientSocket));
	std::cout << "Recieved message code " << message.code() << ".\n";
	return message.code();
}

void StaticTCPServer::sendMessage(SOCKET clientSocket, const google::protobuf::Message &message)
{
	sendWithSize(clientSocket, message.SerializeAsString());
	std::cout << "Sent message " << typeid(message).name() << ".\n";
}