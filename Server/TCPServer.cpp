#pragma comment(lib, "ws2_32.lib")
#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#endif

#include "TCPServer.h"
#include <Windows.h>

TCPServer::TCPServer()
{	// Initialize usage of WinSock2
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		throw SocketException(__FUNCTION__);

	// Initialize server socket
	serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (serverSocket == INVALID_SOCKET)
		throw SocketException(__FUNCTION__);
}

void TCPServer::bind(const std::string &ip, short port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);			// port that server will listen for
	sa.sin_family = AF_INET;	// must be AF_INET
	sa.sin_addr.s_addr = inet_addr(ip.c_str());

	if (::bind(serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw SocketException(__FUNCTION__);
}

void TCPServer::listen(int backlog)
{
	if (::listen(serverSocket, backlog) == SOCKET_ERROR)
		throw SocketException(__FUNCTION__);
}

SOCKET TCPServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET clientSocket = ::accept(serverSocket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
		throw SocketException(__FUNCTION__);

	return clientSocket;
}

int TCPServer::send(SOCKET clientSocket, std::string message)
{
	int bytesSent = ::send(clientSocket, message.c_str(), message.size(), NULL);
	if (bytesSent == SOCKET_ERROR)
		throw SocketException(__FUNCTION__);

	return bytesSent;
}

int TCPServer::sendSize(SOCKET clientSocket, uint32_t size)
{
	int bytesSent = ::send(clientSocket, (const char*)&size, sizeof(uint32_t), NULL);
	if (bytesSent == SOCKET_ERROR)
		throw SocketException(__FUNCTION__);

	return bytesSent;
}

std::string TCPServer::recv(SOCKET clientSocket, int bytes)
{
	char buff[1024];
	if (::recv(clientSocket, buff, bytes, NULL) == SOCKET_ERROR)
		throw SocketException(__FUNCTION__);

	return std::string(buff);
}

uint32_t TCPServer::recvSize(SOCKET clientSocket)
{
	uint32_t buff;
	if (::recv(clientSocket, (char*)&buff, sizeof(uint32_t), NULL) == SOCKET_ERROR)
		throw SocketException(__FUNCTION__);

	return buff;
}

bool TCPServer::isThereData(SOCKET clientSocket)
{
	unsigned long bytes;
	ioctlsocket(clientSocket, FIONREAD, &bytes);

	return bytes != 0;
}

TCPServer::~TCPServer()
{
	::closesocket(serverSocket);
}