#pragma once
#include <list>

template <typename T>
class TSList<T>
{
private:
	std::list<T> list;

public:
	void add(T value);
	template< class... Args >
	void emplace_back(Args&&... args);
	std::list<T>::iterator begin();
};