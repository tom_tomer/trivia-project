#pragma once
#include "protocol.pb.h"
#include <vector>
#include <iostream>

#ifdef ERROR
#undef ERROR
#endif	// ERROR

#ifndef TRACE
#define TRACE(str) std::cout << str
#endif	// TRACELN

#ifndef TRACELN
#define TRACELN(str) std::cout << str << '\n'
#endif	// TRACELN

namespace utils
{
	template<typename Out>
	void split(const std::string &s, char delim, Out result);
	std::vector<std::string> split(const std::string &s, char delim);

	std::vector<int> getPacksId(const google::protobuf::RepeatedPtrField<protocol::PackInfo> &packs);

	// remove whitespace from the begining and the end of the string
	void trim(std::string &str);

	// remove whitespace from the end of the string
	void rtrim(std::string &str);

	// remove whitespace from the beginning of the string
	void ltrim(std::string &str);
}