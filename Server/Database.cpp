#include "Database.h"
#include "Utils.h"
#include "Config.h"
#include <cpplinq.hpp>
#include <sstream>
#include <algorithm>
#include <random>
#include <fstream>

// Re-Declarations
const std::string Database::path = "database.db";

Database &Database::get()
{
	static Database instance;
	return instance;
}

Database::Database() : db(path, SQLite::OPEN_CREATE | SQLite::OPEN_READWRITE)
{
	if (!(db.tableExists("Users")))
		createTableUsers();

	createTablePacks();
	createTableQuestion();

	populatePacksAndQuestions();
}

void Database::createTableUsers()
{
	db.exec("DROP TABLE IF EXISTS Users");
	db.exec( 
		"CREATE TABLE Users"
		"("
			"id			INTEGER		PRIMARY KEY		AUTOINCREMENT,"
			"username	TEXT		NOT NULL,"
			"password	TEXT		NOT NULL"
		")"
	);
}

void Database::createTablePacks()
{
	db.exec("DROP TABLE IF EXISTS Packs");
	db.exec(
		"CREATE TABLE Packs"
		"("
			"id		INTEGER		PRIMARY KEY		AUTOINCREMENT,"
			"name	TEXT		NOT NULL"
		")"
	);
}

void Database::createTableQuestion()
{
	db.exec("DROP TABLE IF EXISTS Questions");
	db.exec(
		"CREATE TABLE Questions"
		"("
			"id				INTEGER		PRIMARY KEY		AUTOINCREMENT,"
			"pack_id		INTEGER		NOT NULL		REFERENCES packs(id),"
			"question		TEXT		NOT NULL,"
			"answer			TEXT		NOT NULL,"
			"fake_answer_1	TEXT		NOT NULL,"
			"fake_answer_2	TEXT		NOT NULL,"
			"fake_answer_3	TEXT		NOT NULL"
		")"
	);
}

void Database::populatePacksAndQuestions()
{
	std::ifstream file(Config::get().get("questions-source"));
	if (!file.is_open())
		throw std::exception("Database: No questions source.");

	std::string line;
	int currPackId = 0;
	while (std::getline(file, line))
	{
		utils::trim(line);

		if (line == "")	// Empty row
			continue;
		else if (line[0] == '-')	// Pack name
		{
			std::string packName = line.substr(1, line.size() - 1);
			utils::trim(packName);
			SQLite::Statement query(db, "INSERT INTO Packs (name) VALUES (?)");
			query.bind(1, packName);
			query.exec();
			currPackId = db.getLastInsertRowid();
		}
		else	// Question and answers
		{
			std::string question = line;
			std::getline(file, line);
			std::string answer = line;
			std::getline(file, line);
			std::string fake1 = line;
			std::getline(file, line);
			std::string fake2 = line;
			std::getline(file, line);
			std::string fake3 = line;

			SQLite::Statement query(db, "INSERT INTO Questions (pack_id, question, answer, fake_answer_1, fake_answer_2, fake_answer_3) VALUES (?, ?, ?, ?, ?, ?)");
			query.bind(1, currPackId);
			query.bind(2, question);
			query.bind(3, answer);
			query.bind(4, fake1);
			query.bind(5, fake2);
			query.bind(6, fake3);
			query.exec();
		}
	}
}

bool Database::userExists(const std::string & username)
{
	SQLite::Statement query(db, "SELECT * FROM Users WHERE username = ?");
	query.bind(1, username);

	if (query.executeStep())
	{
		return true;
	}
	
	return false;
}

bool Database::checkCredentials(const std::string& username, const std::string& password)
{
	SQLite::Statement query(db, "SELECT * FROM Users WHERE username = ?	AND password = ?");
	query.bind(1, username);
	query.bind(2, password);

	if (query.executeStep())
	{
		return true;
	}

	return false;
}

bool Database::addClient(const std::string& username, const std::string& password)
{
	if (!(userExists(username)))
	{
		SQLite::Statement query(db, "INSERT INTO Users (username, password) VALUES (?, ?)");
		query.bind(1, username);
		query.bind(2, password);
		query.exec();
		return true;
	}

	return false;
}

std::string Database::getPackNameById(int packId)
{
	SQLite::Statement query(db, "SELECT name FROM Packs WHERE id = ?");
	query.bind(1, packId);

	if (query.executeStep())
		return query.getColumn("name");
	else
		throw SQLite::Exception("No pack id found.");
}

void Database::getPacksInfo(protocol::PacksList &list)
{
	SQLite::Statement query(db, "SELECT * FROM Packs");
	while (query.executeStep())
	{
		auto packInfo = list.add_packs();
		packInfo->set_id(query.getColumn("id"));
		packInfo->set_name(query.getColumn("name").getString());
		packInfo->set_question_count(getPackQuestionCount(query.getColumn("id")));
	}
}

int Database::getPackQuestionCount(int packId)
{
	SQLite::Statement query(db, "SELECT count(id) FROM Questions WHERE pack_id = ?");
	query.bind(1, packId);

	if (query.executeStep())
		return query.getColumn(0);
	else
		throw SQLite::Exception("No pack id found.");
}

void Database::getRandomQuestions(std::vector<std::pair<protocol::QuestionInfo, int>>& questions, const std::vector<int>& packIds)
{
	// Initialize the query
	SQLite::Statement query(db, "SELECT (question, answer, fake_answer_1, fake_answer_2, fake_answer_3) FROM Questions"
								"WHERE ?" 
								"ORDER BY RANDOM() LIMIT ?");
	
	std::stringstream where;
	int i;
	for (i = 0; i < packIds.size() - 1; i++)
	{
		where << "pack_id = " << packIds[i] << " OR ";
	}
	where << "pack_id = " << packIds[i];

	query.bind(1, where.str());
	query.bind(questions.size());

	// Initialize the questionInfos in questions
	for (i = 0; query.executeStep(); i++)
	{
		std::vector<std::string> answers{ query.getColumn("answer").getString(), query.getColumn("fake_answer_1").getString(), query.getColumn("fake_answer_2").getString(), query.getColumn("fake_answer_3").getString() };
		
		std::random_device rd;
		std::mt19937 g(rd());
		std::shuffle(answers.begin(), answers.end(), g);

		questions[i].first.set_answer1(answers[0]);
		questions[i].first.set_answer2(answers[1]);
		questions[i].first.set_answer3(answers[2]);
		questions[i].first.set_answer4(answers[3]);

		questions[i].second = std::distance(std::find(answers.begin(), answers.end(), query.getColumn("answer").getString()), answers.begin());
	}
}
