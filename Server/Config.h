#pragma once
#include <map>
#include <string>

class Config
{
private:
	static const std::string path;

	Config();
	
	std::map<std::string, std::string> values;

public:
	Config(Config const&) = delete;
	void operator=(Config const&) = delete;

	// Returns the instance of the class
	static Config &get();
	std::string get(const std::string &key);
};