#include "Room.h"
#include "Database.h"
#include "StaticTCPServer.h"
#include "Utils.h"

using Server = StaticTCPServer;

int Room::counter = 0;

Room::Room(SOCKET owner, const protocol::RoomInfo &roomInfo)
	: name(roomInfo.name()), owner(owner), maxUsers(roomInfo.max_users()),
	packsId(utils::getPacksId(roomInfo.packs())), questions(roomInfo.question_count())
{
	Database::get().getRandomQuestions(questions, packsId);

	this->id = counter;
	counter++;
	
	Game::get().clients.find(owner)->second.room = this;
}

void Room::getRoomInfo(protocol::RoomInfo &roomInfo)
{
	roomInfo.set_id(id);
	roomInfo.set_name(name);
	roomInfo.set_admin(Game::get().clients.find(owner)->second.getUsername());
	roomInfo.set_max_users(maxUsers);
	roomInfo.set_curr_users_num(users.size());
	roomInfo.set_question_count(questions.size());

	for (int packId : packsId)
	{
		auto pack = roomInfo.add_packs();
		pack->set_name(Database::get().getPackNameById(packId));
	}
}

void Room::addUser(SOCKET clientSocket)
{
	users.insert(clientSocket);
	Game::get().clients.find(clientSocket)->second.room = this;
}

int Room::getId()
{
	return id;
}

SOCKET Room::getOwner()
{
	return owner;
}

void Room::removeUser(SOCKET clientSocket)
{
	users.erase(clientSocket);
	Game::get().clients.find(clientSocket)->second.room = nullptr;
}

void Room::startGame()
{
	// Send START_GAME to all users in the sender's room
	for (SOCKET socket : users)
	{
		if (socket != owner)
			Server::sendMessageCode(socket, protocol::ServerMessageCode::START_GAME);
	}

	// Wait for 10 seconds
	Sleep(10000);
}
