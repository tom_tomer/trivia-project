#pragma once
#include "protocol.pb.h"
#include "Game.h"
#include <list>
#include <WinSock2.h>
#include <string>
#include <memory>

class Game;
class User;

class Room
{
public:
	enum Status
	{
		OPEN,
		IN_GAME
	};

private:
	static int counter;
	int id;
	Status status = OPEN;

	const std::string name;
	const SOCKET owner;
	const int maxUsers;
	const std::vector<int> packsId;

	std::vector<std::pair<protocol::QuestionInfo, int>> questions;

public:
	std::set<SOCKET> users;

	Room(SOCKET owner, const protocol::RoomInfo &roomInfo);
	void getRoomInfo(protocol::RoomInfo &roomInfo) ;

	void addUser(SOCKET clientSocket);
	int getId();
	SOCKET getOwner();
	void removeUser(SOCKET clientSocket);

	void startGame();
};